import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { catchError, from, take, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { NewDiagnoseInterface } from '../interfaces/diagnose.interface';
import { DiagnoseEntity } from '../models/diagnose.entity';

@Injectable()
export class DiagnoseService {
  constructor(
    @InjectRepository(DiagnoseEntity)
    private readonly diagnoseRepository: Repository<DiagnoseEntity>,
  ) {}

  // method creates multiple diagnose entities in chunks of 200
  public async createMultiple(diagnose: NewDiagnoseInterface[]) {
    await this.diagnoseRepository.save(diagnose, { chunk: 200 });
  }

  // method finds diagnose by id
  public findById(id: string) {
    return from(this.diagnoseRepository.findOne(id)).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method searches for diagnose by name or abbreviation
  public searchByNameOrAbbreviation(searchTerm: string) {
    return from(
      this.diagnoseRepository
        .createQueryBuilder('diagnose')
        .select([
          'diagnose.id',
          'diagnose.name',
          'diagnose.code',
          'diagnose.abbreviation',
          'diagnose.description',
        ])
        .where('diagnose.name ILIKE :searchTerm', {
          searchTerm: `%${searchTerm}%`,
        })
        .orWhere('diagnose.abbreviation ILIKE :searchTerm', {
          searchTerm: `%${searchTerm}%`,
        })
        .take(20)
        .getMany(),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
