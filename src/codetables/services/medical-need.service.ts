import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, take, catchError, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { NewMedicalNeedInterface } from '../interfaces/medical-need.interface';
import { MedicalNeedEntity } from '../models/medical-need.entity';

@Injectable()
export class MedicalNeedService {
  constructor(
    @InjectRepository(MedicalNeedEntity)
    private readonly medicalNeedRepository: Repository<MedicalNeedEntity>,
  ) {}

  // method creates multiple medical_need entities in chunks of 200
  public async createMultiple(medicalNeed: NewMedicalNeedInterface[]) {
    await this.medicalNeedRepository.save(medicalNeed, { chunk: 200 });
  }

  // method finds medical_need by id
  public findById(id: string) {
    return from(this.medicalNeedRepository.findOne(id)).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method searches for medical_need by name
  public searchByName(searchTerm: string) {
    return from(
      this.medicalNeedRepository
        .createQueryBuilder('medical_need')
        .select()
        .where('medical_need.zp_nazov ILIKE :searchTerm', {
          searchTerm: `%${searchTerm}%`,
        })
        .take(20)
        .getMany(),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
