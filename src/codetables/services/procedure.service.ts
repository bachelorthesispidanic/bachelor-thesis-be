import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, take, catchError, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { NewProcedureInterface } from '../interfaces/procedure.interface';
import { ProcedureEntity } from '../models/procedure.entity';

@Injectable()
export class ProcedureService {
  constructor(
    @InjectRepository(ProcedureEntity)
    private readonly procedureRepository: Repository<ProcedureEntity>,
  ) {}

  // method creates multiple procedure entities in chunks of 200
  public async createMultiple(procedures: NewProcedureInterface[]) {
    await this.procedureRepository.save(procedures, { chunk: 200 });
  }

  // method finds diagnose by id
  public findById(id: string) {
    return from(this.procedureRepository.findOne(id)).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method searches for procedure by name or abbreviation
  public searchByNameOrAbbreviation(searchTerm: string) {
    return from(
      this.procedureRepository
        .createQueryBuilder('procedure')
        .select([
          'procedure.id',
          'procedure.name',
          'procedure.code',
          'procedure.abbreviation',
          'procedure.description',
        ])
        .where('procedure.name ILIKE :searchTerm', {
          searchTerm: `%${searchTerm}%`,
        })
        .orWhere('procedure.abbreviation ILIKE :searchTerm', {
          searchTerm: `%${searchTerm}%`,
        })
        .take(20)
        .getMany(),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
