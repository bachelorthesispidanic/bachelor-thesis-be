import { HttpService } from '@nestjs/axios';
import { HttpException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, take, catchError, throwError, map, tap } from 'rxjs';
import { Repository } from 'typeorm';
import { NewMedicineInterface } from '../interfaces/medicine.interface';
import { MedicineEntity } from '../models/medicine.entity';

@Injectable()
export class MedicineService {
  constructor(
    @InjectRepository(MedicineEntity)
    private readonly medicineRepository: Repository<MedicineEntity>,
    private httpService: HttpService,
  ) {}

  // method creates multiple medicine entities in chunks of 200
  public async createMultiple(medicines: NewMedicineInterface[]) {
    await this.medicineRepository.save(medicines, { chunk: 200 });
  }

  // method finds medicine by id
  public findById(id: string) {
    return from(this.medicineRepository.findOne(id)).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method searches for medicine by name
  public searchByName(searchTerm: string) {
    return from(
      this.medicineRepository
        .createQueryBuilder('medicine')
        .select()
        .where('medicine.nazov ILIKE :searchTerm', {
          searchTerm: `%${searchTerm}%`,
        })
        .take(20)
        .getMany(),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method gets info url for medicine
  public async getInfoUrl(searchTerm: string): Promise<any> {
    return this.httpService
      .get(`https://www.adc.sk/hladaj?q=${searchTerm}`, {
        headers: {
          Accept: 'text/html',
        },
      })
      .pipe(
        take(1),
        map((res) => ({
          url: `https://www.adc.sk/${
            res.data
              .toString()
              .match(/href='\/databazy(.)+'/g)[0]
              .split("'")[1]
          }`,
        })),
        catchError((err) => {
          console.log(JSON.stringify(err));
          return throwError(() => new NotFoundException('Medicine not found'));
        }),
      );
  }
}
