import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { catchError, from, take, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { NewSexInterface } from '../interfaces/sex.interface';
import { SexEntity } from '../models/sex.entity';

@Injectable()
export class SexService {
  constructor(
    @InjectRepository(SexEntity)
    private readonly sexRepository: Repository<SexEntity>,
  ) {}

  // method finds all
  public async findAll() {
    return this.sexRepository.find();
  }

  // method creates multiple sex entities in chunks of 200
  public async createMultiple(sex: NewSexInterface[]) {
    await this.sexRepository.save(sex, { chunk: 200 });
  }

  // method finds sex entity by id
  public findById(id: string) {
    return from(this.sexRepository.findOne({ where: { id: id } })).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
