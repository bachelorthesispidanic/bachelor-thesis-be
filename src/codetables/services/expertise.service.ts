import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, take, catchError, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { NewExpertiseInterface } from '../interfaces/expertise.interface';
import { ExpertiseEntity } from '../models/expertise.entity';

@Injectable()
export class ExpertiseService {
  constructor(
    @InjectRepository(ExpertiseEntity)
    private readonly expertiseRepository: Repository<ExpertiseEntity>,
  ) {}

  // method creates multiple expertise entities in chunks of 200
  public async createMultiple(expertise: NewExpertiseInterface[]) {
    await this.expertiseRepository.save(expertise, { chunk: 200 });
  }

  // method finds expertise by abbreviation
  public findByAbbreviation(abbreviation: string) {
    return from(
      this.expertiseRepository.findOne({
        where: { abbreviation: abbreviation },
      }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
