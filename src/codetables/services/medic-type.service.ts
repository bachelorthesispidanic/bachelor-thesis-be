import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, take, catchError, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { NewMedicTypeInterface } from '../interfaces/medic-type.interface';
import { MedicTypeEntity } from '../models/medic-type.entity';

@Injectable()
export class MedicTypeService {
  constructor(
    @InjectRepository(MedicTypeEntity)
    private readonly medicTypeRepository: Repository<MedicTypeEntity>,
  ) {}

  // method creates multiple medic_type entities in chunks of 200
  public async createMultiple(medicType: NewMedicTypeInterface[]) {
    await this.medicTypeRepository.save(medicType, { chunk: 200 });
  }

  // method finds medic_type by abbreviation
  public findByAbbreviation(abbreviation: string) {
    return from(
      this.medicTypeRepository.findOne({
        where: { abbreviation: abbreviation },
      }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
