export interface NewExpertiseInterface {
  code: string;
  abbreviation: string;
  name: string;
}

export interface ExpertiseInterface extends NewExpertiseInterface {
  id: string;
}
