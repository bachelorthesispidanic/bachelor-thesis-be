export interface NewMedicineInterface {
  sukl_kod: string;
  nazov: string;
  doplnok: string;
  kod_drz: string;
  kod_statu: string;
  atc_kod: string;
  atc_nazov_sk: string;
  is: string;
  reg_cislo: string;
  expiracia: string;
  vydaj: string;
  typ_reg: string;
  datum_reg: string;
  platnost: string;
  bp: string;
}

export interface MedicineInterface extends NewMedicineInterface {
  id: string;
}
