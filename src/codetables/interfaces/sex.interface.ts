export interface NewSexInterface {
  code: string;
  abbreviation: string;
  name: string;
  nameEn: string;
}

export interface SexInterface extends NewSexInterface {
  id: string;
}
