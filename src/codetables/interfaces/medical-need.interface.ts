export interface NewMedicalNeedInterface {
  zp_id: string;
  zp_kod: string;
  zp_nazov: string;
  zp_nazov1: string;
  zp_doplnok: string;
  zp_doplnok1: string;
  zp_stav_kod: string;
  zp_stav_nazov: string;
  zp_platnost: string;
  kat_kod: string;
  vyr_kod: string;
  vyr_sta_kod: string;
  tr_kod: string;
  tr_nazov: string;
  zas_kod: string;
}

export interface MedicalNeedInterface extends NewMedicalNeedInterface {
  id: string;
}
