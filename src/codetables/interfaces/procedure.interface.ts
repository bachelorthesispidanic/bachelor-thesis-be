export interface NewProcedureInterface {
  code: string;
  abbreviation: string;
  name: string;
  description: string;
}

export interface ProcedureInterface extends NewProcedureInterface {
  id: string;
}
