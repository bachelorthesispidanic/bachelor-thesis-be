export interface NewMedicTypeInterface {
  code: string;
  abbreviation: string;
  name: string;
}

export interface MedicTypeInterface extends NewMedicTypeInterface {
  id: string;
}
