export interface NewDiagnoseInterface {
  code: string;
  abbreviation: string;
  name: string;
  description: string;
}

export interface DiagnoseInterface extends NewDiagnoseInterface {
  id: string;
}
