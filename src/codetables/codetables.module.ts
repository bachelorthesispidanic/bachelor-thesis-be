import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DiagnoseEntity } from './models/diagnose.entity';
import { ExpertiseEntity } from './models/expertise.entity';
import { MedicTypeEntity } from './models/medic-type.entity';
import { MedicalNeedEntity } from './models/medical-need.entity';
import { MedicineEntity } from './models/medicine.entity';
import { ProcedureEntity } from './models/procedure.entity';
import { SexEntity } from './models/sex.entity';
import { DiagnoseService } from './services/diagnose.service';
import { ExpertiseService } from './services/expertise.service';
import { MedicTypeService } from './services/medic-type.service';
import { MedicalNeedService } from './services/medical-need.service';
import { MedicineService } from './services/medicine.service';
import { ProcedureService } from './services/procedure.service';
import { SexService } from './services/sex.service';
import { MedicineController } from './controllers/medicine.controller';
import { MedicalNeedController } from './controllers/medical-need.controller';
import { ProcedureController } from './controllers/procedure.controller';
import { DiagnoseController } from './controllers/diagnose.controller';
import { HttpModule } from '@nestjs/axios';
import { SexController } from './controllers/sex.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      DiagnoseEntity,
      ExpertiseEntity,
      MedicTypeEntity,
      MedicalNeedEntity,
      MedicineEntity,
      ProcedureEntity,
      SexEntity,
    ]),
    HttpModule,
  ],
  providers: [
    DiagnoseService,
    ExpertiseService,
    MedicTypeService,
    MedicalNeedService,
    MedicineService,
    ProcedureService,
    SexService,
  ],
  exports: [
    DiagnoseService,
    ExpertiseService,
    MedicTypeService,
    MedicalNeedService,
    MedicineService,
    ProcedureService,
    SexService,
  ],
  controllers: [
    MedicineController,
    MedicalNeedController,
    ProcedureController,
    DiagnoseController,
    SexController,
  ],
})
export class CodetablesModule {}
