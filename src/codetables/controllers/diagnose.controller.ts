import { Controller, DefaultValuePipe, Get, Query } from '@nestjs/common';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { DiagnoseDto } from '../dtos/diagnose.dto';
import { DiagnoseService } from '../services/diagnose.service';

@ApiTags('Diagnose')
@Controller('diagnose')
export class DiagnoseController {
  constructor(private readonly diagnoseService: DiagnoseService) {}

  @ApiOkResponse({ type: [DiagnoseDto] })
  @Get()
  public searchDiagnose(
    @Query('search', new DefaultValuePipe('')) search: string,
  ) {
    return this.diagnoseService.searchByNameOrAbbreviation(search);
  }
}
