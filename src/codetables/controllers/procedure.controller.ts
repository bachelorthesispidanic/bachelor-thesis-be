import { Controller, DefaultValuePipe, Get, Query } from '@nestjs/common';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { ProcedureDto } from '../dtos/procedure.dto';
import { ProcedureService } from '../services/procedure.service';

@ApiTags('Procedure')
@Controller('procedure')
export class ProcedureController {
  constructor(private readonly procedureService: ProcedureService) {}

  @ApiOkResponse({ type: [ProcedureDto] })
  @Get()
  public searchProcedure(
    @Query('search', new DefaultValuePipe('')) search: string,
  ) {
    return this.procedureService.searchByNameOrAbbreviation(search);
  }
}
