import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { SexDto } from '../dtos/sex.dto';
import { SexService } from '../services/sex.service';

@ApiTags('Sex')
@Controller('sex')
export class SexController {
  constructor(private readonly sexService: SexService) {}

  @ApiOkResponse({ type: [SexDto] })
  @Get()
  getAll() {
    return this.sexService.findAll();
  }
}
