import { Controller, DefaultValuePipe, Get, Query } from '@nestjs/common';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { MedicalNeedDto } from '../dtos/medical-need.dto';
import { MedicalNeedService } from '../services/medical-need.service';

@ApiTags('MedicalNeed')
@Controller('medical-need')
export class MedicalNeedController {
  constructor(private readonly medicalNeedService: MedicalNeedService) {}

  @ApiOkResponse({ type: [MedicalNeedDto] })
  @Get()
  public searchMedicalNeed(
    @Query('search', new DefaultValuePipe('')) search: string,
  ) {
    return this.medicalNeedService.searchByName(search);
  }
}
