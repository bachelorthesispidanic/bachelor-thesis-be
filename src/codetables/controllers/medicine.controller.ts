import { Controller, DefaultValuePipe, Get, Query } from '@nestjs/common';
import { ApiTags, ApiOkResponse, ApiProperty } from '@nestjs/swagger';
import { InfoUrlResponseDto, MedicineDto } from '../dtos/medicine.dto';
import { MedicineService } from '../services/medicine.service';

@ApiTags('Medicine')
@Controller('medicine')
export class MedicineController {
  constructor(private readonly medicineService: MedicineService) {}

  @ApiOkResponse({ type: [MedicineDto] })
  @Get()
  public searchMedicine(
    @Query('search', new DefaultValuePipe('')) search: string,
  ) {
    return this.medicineService.searchByName(search);
  }

  @ApiOkResponse({ type: InfoUrlResponseDto })
  @Get('info-url')
  public async getInfoUrl(
    @Query('search', new DefaultValuePipe('')) search: string,
  ) {
    return await this.medicineService.getInfoUrl(search);
  }
}
