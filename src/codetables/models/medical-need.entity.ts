import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('medical_need')
export class MedicalNeedEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  zp_id: string;

  @Column()
  zp_kod: string;

  @Column()
  zp_nazov: string;

  @Column()
  zp_nazov1: string;

  @Column()
  zp_doplnok: string;

  @Column()
  zp_doplnok1: string;

  @Column()
  zp_stav_kod: string;

  @Column()
  zp_stav_nazov: string;

  @Column()
  zp_platnost: string;

  @Column()
  kat_kod: string;

  @Column()
  vyr_kod: string;

  @Column()
  vyr_sta_kod: string;

  @Column()
  tr_kod: string;

  @Column()
  tr_nazov: string;

  @Column()
  zas_kod: string;
}
