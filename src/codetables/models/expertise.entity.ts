import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('expertise')
export class ExpertiseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  code: string;

  @Column()
  abbreviation: string;

  @Column()
  name: string;
}
