import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('procedure')
export class ProcedureEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  code: string;

  @Column()
  abbreviation: string;

  @Column()
  name: string;

  @Column()
  description: string;
}
