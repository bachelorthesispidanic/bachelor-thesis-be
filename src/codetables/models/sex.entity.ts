import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('sex')
export class SexEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  code: string;

  @Column()
  abbreviation: string;

  @Column()
  name: string;

  @Column()
  nameEn: string;
}
