import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('medic_type')
export class MedicTypeEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  code: string;

  @Column()
  abbreviation: string;

  @Column()
  name: string;
}
