import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('diagnose')
export class DiagnoseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  code: string;

  @Column()
  abbreviation: string;

  @Column()
  name: string;

  @Column()
  description: string;
}
