import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('medicine')
export class MedicineEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  sukl_kod: string;

  @Column()
  nazov: string;

  @Column()
  doplnok: string;

  @Column()
  kod_drz: string;

  @Column()
  kod_statu: string;

  @Column()
  atc_kod: string;

  @Column()
  atc_nazov_sk: string;

  @Column()
  is: string;

  @Column()
  reg_cislo: string;

  @Column()
  expiracia: string;

  @Column()
  vydaj: string;

  @Column()
  typ_reg: string;

  @Column()
  datum_reg: string;

  @Column()
  platnost: string;

  @Column()
  bp: string;
}
