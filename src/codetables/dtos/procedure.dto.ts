import { ApiProperty } from '@nestjs/swagger';

export class ProcedureDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  code: string;

  @ApiProperty()
  abbreviation: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  description: string;
}
