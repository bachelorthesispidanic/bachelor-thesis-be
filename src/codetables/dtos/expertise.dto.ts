import { ApiProperty } from '@nestjs/swagger';

export class ExpertiseDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  code: string;

  @ApiProperty()
  abbreviation: string;

  @ApiProperty()
  name: string;
}
