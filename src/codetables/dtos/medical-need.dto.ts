import { ApiProperty } from '@nestjs/swagger';

export class MedicalNeedDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  zp_id: string;

  @ApiProperty()
  zp_kod: string;

  @ApiProperty()
  zp_nazov: string;

  @ApiProperty()
  zp_nazov1: string;

  @ApiProperty()
  zp_doplnok: string;

  @ApiProperty()
  zp_doplnok1: string;

  @ApiProperty()
  zp_stav_kod: string;

  @ApiProperty()
  zp_stav_nazov: string;

  @ApiProperty()
  zp_platnost: string;

  @ApiProperty()
  kat_kod: string;

  @ApiProperty()
  vyr_kod: string;

  @ApiProperty()
  vyr_sta_kod: string;

  @ApiProperty()
  tr_kod: string;

  @ApiProperty()
  tr_nazov: string;

  @ApiProperty()
  zas_kod: string;
}
