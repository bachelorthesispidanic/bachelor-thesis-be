import { ApiProperty } from '@nestjs/swagger';

export class MedicTypeDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  code: string;

  @ApiProperty()
  abbreviation: string;

  @ApiProperty()
  name: string;
}
