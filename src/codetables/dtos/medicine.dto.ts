import { ApiProperty } from '@nestjs/swagger';

export class MedicineDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  sukl_kod: string;

  @ApiProperty()
  nazov: string;

  @ApiProperty()
  doplnok: string;

  @ApiProperty()
  kod_drz: string;

  @ApiProperty()
  kod_statu: string;

  @ApiProperty()
  atc_kod: string;

  @ApiProperty()
  atc_nazov_sk: string;

  @ApiProperty()
  is: string;

  @ApiProperty()
  reg_cislo: string;

  @ApiProperty()
  expiracia: string;

  @ApiProperty()
  vydaj: string;

  @ApiProperty()
  typ_reg: string;

  @ApiProperty()
  datum_reg: string;

  @ApiProperty()
  platnost: string;

  @ApiProperty()
  bp: string;
}

export class InfoUrlResponseDto {
  @ApiProperty()
  url: string;
}
