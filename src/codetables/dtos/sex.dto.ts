import { ApiProperty } from '@nestjs/swagger';

export class SexDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  code: string;

  @ApiProperty()
  abbreviation: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  nameEn: string;
}
