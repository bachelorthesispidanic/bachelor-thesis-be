import { ApiProperty } from '@nestjs/swagger';

export class DiagnoseDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  code: string;

  @ApiProperty()
  abbreviation: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  description: string;
}
