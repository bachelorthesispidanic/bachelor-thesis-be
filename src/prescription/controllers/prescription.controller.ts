import {
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiTags,
  ApiCreatedResponse,
  ApiUnauthorizedResponse,
  ApiOkResponse,
} from '@nestjs/swagger';
import {
  NewPrescriptionDto,
  PaginatedPrescriptionResponseDto,
} from '../dtos/prescription.dto';
import { PrescriptionService } from '../services/prescription.service';
import { PrescriptionResponseDto } from '../dtos/prescription.dto';

@ApiTags('Prescription')
@Controller('prescription')
export class PrescriptionController {
  constructor(private readonly prescriptionService: PrescriptionService) {}

  @ApiCreatedResponse({ type: PrescriptionResponseDto })
  @ApiUnauthorizedResponse()
  @UseGuards(AuthGuard('jwt'))
  @Post('new')
  public createPrescription(
    @Req() req,
    @Body() prescription: NewPrescriptionDto,
  ) {
    return this.prescriptionService.createPrescription(
      prescription,
      req.user.userId,
    );
  }

  @ApiOkResponse({ type: PrescriptionResponseDto })
  @ApiUnauthorizedResponse()
  @UseGuards(AuthGuard('jwt'))
  @Get('patient/:id')
  public findByPatientId(
    @Req() req,
    @Param('id') id: string,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    return this.prescriptionService.findByPatientId({ page, limit }, id);
  }

  @ApiOkResponse({ type: PaginatedPrescriptionResponseDto })
  @ApiUnauthorizedResponse()
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  public findById(@Req() req, @Param('id') id: string) {
    return this.prescriptionService.findById(id);
  }

  @ApiOkResponse({ type: PrescriptionResponseDto })
  @ApiUnauthorizedResponse()
  @UseGuards(AuthGuard('jwt'))
  @Get('record/:id')
  public findByRecordId(@Req() req, @Param('id') id: string) {
    return this.prescriptionService.findByRecordId(id);
  }
}
