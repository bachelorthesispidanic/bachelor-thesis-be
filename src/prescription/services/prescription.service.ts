import { BadRequestException, HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import { map, concatMap, catchError, throwError, from, take, of } from 'rxjs';
import { MedicalNeedService } from 'src/codetables/services/medical-need.service';
import { MedicineService } from 'src/codetables/services/medicine.service';
import { EmployeeService } from 'src/employee/services/employee.service';
import { PatientService } from 'src/patient/services/patient.service';
import { RecordService } from 'src/record/services/record.service';
import { Repository } from 'typeorm';
import { NewPrescriptionInterface } from '../interfaces/prescription.interface';
import { PrescriptionEntity } from '../models/prescription.entity';

@Injectable()
export class PrescriptionService {
  constructor(
    @InjectRepository(PrescriptionEntity)
    private readonly prescriptionRepository: Repository<PrescriptionEntity>,
    private readonly employeeService: EmployeeService,
    private readonly patientService: PatientService,
    private readonly medicineService: MedicineService,
    private readonly medicalNeedService: MedicalNeedService,
    private readonly recordService: RecordService,
  ) {}

  // method creates prescription entity
  public createPrescription(
    prescription: NewPrescriptionInterface,
    employeeAuthId: string,
  ) {
    console.log(prescription);
    const newPrescription = new PrescriptionEntity();

    return this.employeeService.findByAuthUserId(employeeAuthId).pipe(
      map((employee) => (newPrescription.employee = employee)),
      concatMap(() =>
        this.patientService
          .findById(prescription.patientId)
          .pipe(map((patient) => (newPrescription.patient = patient))),
      ),
      concatMap(() => {
        if (prescription.recordId) {
          return this.recordService
            .findById(prescription.recordId)
            .pipe(map((record) => (newPrescription.record = record)));
        } else {
          newPrescription.record = null;
          return of(null);
        }
      }),
      concatMap(() => {
        console.log(newPrescription);
        if (prescription.medicineId) {
          return this.medicineService.findById(prescription.medicineId).pipe(
            map((medicine) => {
              newPrescription.medicalNeed = null;
              return (newPrescription.medicine = medicine);
            }),
          );
        } else if (prescription.medicalNeedId) {
          return this.medicalNeedService
            .findById(prescription.medicalNeedId)
            .pipe(
              map((medicalNeed) => {
                newPrescription.medicine = null;
                return (newPrescription.medicalNeed = medicalNeed);
              }),
            );
        } else {
          return throwError(
            () =>
              new BadRequestException('No medicine or medical need selected'),
          );
        }
      }),
      concatMap(() => this.savePrescription(newPrescription)),
      map((prescription) => prescription),
    );
  }

  // method finds prescription by id
  public findById(id: string) {
    return from(
      this.prescriptionRepository.findOne({
        where: { id: id },
        relations: ['employee', 'patient', 'record', 'medicine', 'medicalNeed'],
      }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method finds prescription by patientId
  public findByPatientId(options: IPaginationOptions, patientId: string) {
    return from(
      paginate(
        this.prescriptionRepository
          .createQueryBuilder('prescription')
          .leftJoinAndSelect('prescription.employee', 'employee')
          .leftJoinAndSelect('prescription.patient', 'patient')
          .leftJoinAndSelect('prescription.record', 'record')
          .leftJoinAndSelect('prescription.medicine', 'medicine')
          .leftJoinAndSelect('prescription.medicalNeed', 'medicalNeed')
          .where('patient.id = :patientId', { patientId: patientId })
          .orderBy('prescription.createdAt', 'DESC'),
        options,
      ),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method finds prescription by recordId
  public findByRecordId(recordId: string) {
    return from(
      this.prescriptionRepository.find({
        where: { record: { id: recordId } },
        relations: ['employee', 'patient', 'medicine', 'medicalNeed'],
      }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  /*
   ********************
   * utility functions*
   ********************
   */

  // function to save patient entity
  private savePrescription(prescription: PrescriptionEntity) {
    return from(this.prescriptionRepository.save(prescription)).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
