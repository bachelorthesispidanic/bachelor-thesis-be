export interface NewPrescriptionInterface {
  patientId: string;
  recordId?: string;
  medicineId?: string;
  medicalNeedId?: string;
}
