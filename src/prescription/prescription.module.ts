import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PrescriptionEntity } from './models/prescription.entity';
import { PrescriptionService } from './services/prescription.service';
import { PrescriptionController } from './controllers/prescription.controller';
import { CodetablesModule } from 'src/codetables/codetables.module';
import { EmployeeModule } from 'src/employee/employee.module';
import { PatientModule } from 'src/patient/patient.module';
import { RecordModule } from 'src/record/record.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([PrescriptionEntity]),
    CodetablesModule,
    PatientModule,
    EmployeeModule,
    RecordModule,
  ],
  providers: [PrescriptionService],
  controllers: [PrescriptionController],
})
export class PrescriptionModule {}
