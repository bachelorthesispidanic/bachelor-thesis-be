import { MedicalNeedEntity } from 'src/codetables/models/medical-need.entity';
import { MedicineEntity } from 'src/codetables/models/medicine.entity';
import { EmployeeEntity } from 'src/employee/models/employee.entity';
import { PatientEntity } from 'src/patient/models/patient.entity';
import { RecordEntity } from 'src/record/models/record.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Column,
} from 'typeorm';

@Entity('prescription')
export class PrescriptionEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'timestamp',
    default: null,
  })
  pickedUpAt: Date;

  @ManyToOne(() => EmployeeEntity, { eager: true })
  @JoinColumn({ name: 'employeeId' })
  employee: EmployeeEntity;

  @ManyToOne(() => PatientEntity, { eager: true })
  @JoinColumn({ name: 'patientId' })
  patient: PatientEntity;

  @ManyToOne(() => RecordEntity, { eager: true, nullable: true })
  @JoinColumn({ name: 'recordId' })
  record: RecordEntity;

  @ManyToOne(() => MedicineEntity, { eager: true, nullable: true })
  @JoinColumn({ name: 'medicineId' })
  medicine: MedicineEntity;

  @ManyToOne(() => MedicalNeedEntity, { eager: true, nullable: true })
  @JoinColumn({ name: 'medicalNeedId' })
  medicalNeed: MedicalNeedEntity;

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    onUpdate: 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    default: null,
  })
  deletedAt: Date;
}
