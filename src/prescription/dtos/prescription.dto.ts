import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsOptional } from 'class-validator';
import { MedicalNeedDto } from 'src/codetables/dtos/medical-need.dto';
import { MedicineDto } from 'src/codetables/dtos/medicine.dto';
import { MetaDto } from 'src/common/common.dto';
import { EmployeeResponseDto } from 'src/employee/dtos/employee.dto';
import { PatientResponseDto } from 'src/patient/dtos/patient.dto';
import { RecordResponseDto } from 'src/record/dtos/record.dto';

export class NewPrescriptionDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  patientId: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  recordId?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  medicineId?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  medicalNeedId?: string;
}

export class PrescriptionResponseDto {
  @ApiProperty()
  id: string;

  employee: EmployeeResponseDto;

  @ApiProperty()
  patient: PatientResponseDto;

  @ApiProperty()
  record: RecordResponseDto;

  @ApiProperty()
  medicalNeed: MedicalNeedDto;

  @ApiProperty()
  medicine: MedicineDto;

  @ApiProperty()
  pickedUpAt: string;

  @ApiProperty()
  createdAt: string;

  @ApiProperty()
  updatedAt: string;

  @ApiProperty()
  deletedAt: string;
}

export class PaginatedPrescriptionResponseDto {
  @ApiProperty()
  items: PrescriptionResponseDto[];

  @ApiProperty()
  meta: MetaDto;
}
