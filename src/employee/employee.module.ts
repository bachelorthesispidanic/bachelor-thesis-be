import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmployeeEntity } from './models/employee.entity';
import { EmployeeController } from './controllers/employee.controller';
import { EmployeeService } from './services/employee.service';
import { AuthModule } from 'src/auth/auth.module';
import { CodetablesModule } from 'src/codetables/codetables.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([EmployeeEntity]),
    AuthModule,
    CodetablesModule,
  ],
  controllers: [EmployeeController],
  providers: [EmployeeService],
  exports: [EmployeeService],
})
export class EmployeeModule {}
