import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsOptional, IsEmail } from 'class-validator';
import { ExpertiseDto } from 'src/codetables/dtos/expertise.dto';
import { MedicTypeDto } from 'src/codetables/dtos/medic-type.dto';

export class NewEmployeeDto {
  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  titlesBeforeName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  lastName: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  titlesAfterName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  code: string;
}

export class EmployeeResponseDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  titlesBeforeName: string;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  titlesAfterName: string;

  @ApiProperty()
  code: string;

  @ApiProperty()
  createdAt: string;

  @ApiProperty()
  updatedAt: string;

  @ApiProperty()
  deletedAt: string;

  @ApiProperty()
  medicType: MedicTypeDto;

  @ApiProperty()
  expertise: ExpertiseDto;
}
