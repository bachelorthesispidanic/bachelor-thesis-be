import { AuthUserEntity } from 'src/auth/models/auth-user.entity';
import { ExpertiseEntity } from 'src/codetables/models/expertise.entity';
import { MedicTypeEntity } from 'src/codetables/models/medic-type.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('employee')
export class EmployeeEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  titlesBeforeName: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ nullable: true })
  titlesAfterName: string;

  @Column()
  code: string;

  @ManyToOne(() => MedicTypeEntity, { eager: true })
  @JoinColumn({ name: 'medicTypeId' })
  medicType: MedicTypeEntity;

  @ManyToOne(() => ExpertiseEntity, { eager: true })
  @JoinColumn({ name: 'expertiseId' })
  expertise: ExpertiseEntity;

  @OneToOne(() => AuthUserEntity)
  @JoinColumn({ name: 'authUserId' })
  authUser: AuthUserEntity;

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    onUpdate: 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    default: null,
  })
  deletedAt: Date;
}
