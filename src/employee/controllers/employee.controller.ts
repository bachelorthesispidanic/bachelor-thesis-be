import { Body, Controller, Post } from '@nestjs/common';
import { NewEmployeeDto } from '../dtos/employee.dto';
import { EmployeeService } from '../services/employee.service';

@Controller('employee')
export class EmployeeController {
  constructor(private readonly employeeService: EmployeeService) {}

  @Post('new')
  createNewEmployee(@Body() employeeData: NewEmployeeDto) {
    return this.employeeService.createNewEmployee(employeeData);
  }
}
