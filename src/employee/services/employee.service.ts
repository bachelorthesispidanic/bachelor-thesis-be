import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, take, catchError, throwError, concatMap, map } from 'rxjs';
import { AuthService } from 'src/auth/services/auth.service';
import { ExpertiseService } from 'src/codetables/services/expertise.service';
import { MedicTypeService } from 'src/codetables/services/medic-type.service';
import { Repository } from 'typeorm';
import { NewEmployeeInterface } from '../interfaces/employee.interface';
import { EmployeeEntity } from '../models/employee.entity';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(EmployeeEntity)
    private readonly employeeRepository: Repository<EmployeeEntity>,
    private readonly authService: AuthService,
    private readonly expertiseService: ExpertiseService,
    private readonly medicTypeService: MedicTypeService,
  ) {}

  // method to get all employees
  public async findAll() {
    return await this.employeeRepository.find({ relations: ['authUser'] });
  }

  // method creates new employee entity
  public createNewEmployee(employeeData: NewEmployeeInterface) {
    //   sample A codes A71235001 A87912063 A62141303
    const newEmployee = new EmployeeEntity();
    newEmployee.titlesBeforeName = employeeData.titlesBeforeName
      ? employeeData.titlesBeforeName + ' '
      : null;
    newEmployee.titlesAfterName = employeeData.titlesAfterName
      ? ', ' + employeeData.titlesAfterName
      : null;
    newEmployee.firstName = employeeData.firstName;
    newEmployee.lastName = employeeData.lastName;
    newEmployee.code = employeeData.code;

    const expertiseAbbreviation = employeeData.code.substring(
      employeeData.code.length - 3,
    );
    const medicTypeAbbreviation = employeeData.code.substring(0, 1);

    return this.expertiseService.findByAbbreviation(expertiseAbbreviation).pipe(
      map((expertise) => {
        newEmployee.expertise = expertise;
      }),
      concatMap(() =>
        this.medicTypeService.findByAbbreviation(medicTypeAbbreviation),
      ),
      map((medicType) => {
        newEmployee.medicType = medicType;
      }),
      concatMap(() => this.authService.createAuthUser(employeeData.email)),
      map((authUser) => {
        newEmployee.authUser = authUser;
      }),
      concatMap(() => this.saveEmployee(newEmployee)),
      map((employee) => {
        delete employee.authUser.passwordHash;
        delete employee.authUser.passwordSalt;
        return employee;
      }),
    );
  }

  // method finds employee by id
  public findById(id: string) {
    return from(
      this.employeeRepository.findOne({
        where: { id: id },
      }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method finds employee by authUserId
  public findByAuthUserId(authUserId: string) {
    return from(
      this.employeeRepository.findOne({
        where: { authUser: { id: authUserId } },
      }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  /*
   ********************
   * utility functions*
   ********************
   */

  // function to save employee entity
  private saveEmployee(employee: EmployeeEntity) {
    return from(this.employeeRepository.save(employee)).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
