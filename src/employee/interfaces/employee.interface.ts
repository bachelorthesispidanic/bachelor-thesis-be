export interface NewEmployeeInterface {
  email: string;
  titlesBeforeName?: string;
  firstName: string;
  lastName: string;
  titlesAfterName?: string;
  code: string;
}
