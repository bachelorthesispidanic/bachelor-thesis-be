import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { from } from 'rxjs';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  sendVerificationCode(email: string) {
    return from(
      this.mailerService.sendMail({
        to: email,
        subject: '',
        template: 'verification-code',
        context: {
          name: '',
        },
      }),
    );
  }
}
