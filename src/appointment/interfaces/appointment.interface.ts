export interface NewWebAppointmentInterface {
  reason: string;
  appointmentTime: Date;
  employeeId: string;
}

export interface NewKioskAppointmentInterface {
  reason: string;
  appointmentTime: Date;
  identificationNumber: string;
  employeeId: string;
}

export interface NewDoctorAppointmentInterface {
  reason: string;
  appointmentTime: Date;
  identificationNumber: string;
}
