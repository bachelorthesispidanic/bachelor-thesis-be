import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { map, concatMap, from, take, catchError, throwError, of } from 'rxjs';
import { EmployeeService } from 'src/employee/services/employee.service';
import { PatientService } from 'src/patient/services/patient.service';
import { Repository } from 'typeorm';
import {
  NewDoctorAppointmentInterface,
  NewKioskAppointmentInterface,
  NewWebAppointmentInterface,
} from '../interfaces/appointment.interface';
import { AppointmentEntity } from '../models/appointment.entity';

@Injectable()
export class AppointmentService {
  constructor(
    @InjectRepository(AppointmentEntity)
    private readonly appointmentRepository: Repository<AppointmentEntity>,
    private readonly patientService: PatientService,
    private readonly employeeService: EmployeeService,
  ) {}

  // method creates appointment entity from web
  public createWebAppointment(
    appointment: NewWebAppointmentInterface,
    authUserId: string,
  ) {
    const newAppointment = new AppointmentEntity();
    newAppointment.reason = appointment.reason;
    newAppointment.appointmentTime = appointment.appointmentTime;

    return this.patientService.findByAuthUserId(authUserId).pipe(
      map((patient) => (newAppointment.patient = patient)),
      concatMap(() => this.employeeService.findById(appointment.employeeId)),
      map((employee) => (newAppointment.employee = employee)),
      concatMap(() => this.saveAppointment(newAppointment)),
      map((appointment) => appointment),
    );
  }

  // method creates appointment entity from kiosk
  public createKioskAppointment(appointment: NewKioskAppointmentInterface) {
    const newAppointment = new AppointmentEntity();
    newAppointment.reason = appointment.reason;
    newAppointment.appointmentTime = appointment.appointmentTime;
    newAppointment.confirmed = true;

    return this.employeeService.findById(appointment.employeeId).pipe(
      map((employee) => (newAppointment.employee = employee)),
      concatMap(() =>
        this.patientService.findByIdentificationNumber(
          appointment.identificationNumber,
        ),
      ),
      map((patient) => (newAppointment.patient = patient)),
      concatMap(() => this.saveAppointment(newAppointment)),
      map((appointment) => appointment),
    );
  }

  // method creates doctor appointment
  public createDoctorAppointment(
    appointment: NewDoctorAppointmentInterface,
    authUserId: string,
  ) {
    const newAppointment = new AppointmentEntity();
    newAppointment.reason = appointment.reason;
    newAppointment.appointmentTime = appointment.appointmentTime;
    newAppointment.confirmed = true;

    return this.employeeService.findByAuthUserId(authUserId).pipe(
      map((employee) => (newAppointment.employee = employee)),
      concatMap(() =>
        this.patientService.findByIdentificationNumber(
          appointment.identificationNumber,
        ),
      ),
      map((patient) => (newAppointment.patient = patient)),
      concatMap(() => this.saveAppointment(newAppointment)),
      map((appointment) => appointment),
    );
  }

  // method finds all appointments for a specific employee for day
  public findAllByEmployeeIdForDay(authUserId: string) {
    const day = new Date();
    return this.employeeService.findByAuthUserId(authUserId).pipe(
      concatMap((employee) => {
        return from(
          this.appointmentRepository.find({
            where: {
              employeeId: employee.id,
              appointmentTime: {
                date: day,
              },
              confirmed: true,
            },
            relations: ['patient'],
          }),
        );
      }),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method for accepting appointment
  public acceptAppointment(appointmentId: string) {
    return from(
      this.appointmentRepository.update(
        { id: appointmentId },
        { confirmed: true },
      ),
    ).pipe(
      take(1),
      concatMap((appointment) => {
        // TODO send email to patient
        return of(appointment);
      }),
      map(() => 'OK'),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method for rejecting appointment
  public rejectAppointment(appointmentId: string) {
    return from(
      this.appointmentRepository.update(
        { id: appointmentId },
        { confirmed: false },
      ),
    ).pipe(
      take(1),
      concatMap((appointment) => {
        // TODO send email to patient
        return of(appointment);
      }),
      map(() => 'OK'),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  /*
   ********************
   * utility functions*
   ********************
   */

  // function to save appointment entity
  private saveAppointment(appointment: AppointmentEntity) {
    return from(this.appointmentRepository.save(appointment)).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
