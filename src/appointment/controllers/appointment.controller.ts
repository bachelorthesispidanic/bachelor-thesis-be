import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  NewDoctorAppointmentDto,
  NewKioskAppointmentDto,
} from '../dtos/appointment.dto';
import { AppointmentService } from '../services/appointment.service';

@Controller('appointment')
export class AppointmentController {
  constructor(private readonly appointmentService: AppointmentService) {}

  @Post('kiosk')
  public createKioskAppointment(@Body() appointment: NewKioskAppointmentDto) {
    return this.appointmentService.createKioskAppointment(appointment);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('web')
  public createWebAppointment(
    @Req() req,
    @Body() appointment: NewKioskAppointmentDto,
  ) {
    return this.appointmentService.createWebAppointment(
      appointment,
      req.user.userId,
    );
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('doctor')
  public createDoctorAppointment(
    @Req() req,
    @Body() appointment: NewDoctorAppointmentDto,
  ) {
    return this.appointmentService.createDoctorAppointment(
      appointment,
      req.user.userId,
    );
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('employee')
  public findByEmployeeId(@Req() req) {
    return this.appointmentService.findAllByEmployeeIdForDay(req.user.userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post(':id/confirm')
  public acceptAppointment(@Param('id') id: string) {
    return this.appointmentService.acceptAppointment(id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post(':id/reject')
  public rejectAppointment(@Param('id') id: string) {
    return this.appointmentService.rejectAppointment(id);
  }
}
