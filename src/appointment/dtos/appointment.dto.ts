import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class NewWebAppointmentDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  reason: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  appointmentTime: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  employeeId: string;
}

export class NewKioskAppointmentDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  reason: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  appointmentTime: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  identificationNumber: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  employeeId: string;
}

export class NewDoctorAppointmentDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  reason: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  appointmentTime: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  identificationNumber: string;
}

export class AppointmentResponeDto {}
