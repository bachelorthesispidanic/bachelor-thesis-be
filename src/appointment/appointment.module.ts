import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmployeeModule } from 'src/employee/employee.module';
import { PatientModule } from 'src/patient/patient.module';
import { AppointmentController } from './controllers/appointment.controller';
import { AppointmentEntity } from './models/appointment.entity';
import { AppointmentService } from './services/appointment.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([AppointmentEntity]),
    PatientModule,
    EmployeeModule,
  ],
  controllers: [AppointmentController],
  providers: [AppointmentService],
})
export class AppointmentModule {}
