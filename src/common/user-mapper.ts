import { map } from 'rxjs';
import { AuthUserInterface } from 'src/auth/interfaces/auth.interface';

export const secureUserResponse = <T = any>() =>
  map((user: AuthUserInterface) => {
    delete user.passwordHash;
    delete user.passwordSalt;
    return user;
  });
