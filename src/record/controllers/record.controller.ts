import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiTags,
  ApiCreatedResponse,
  ApiUnauthorizedResponse,
  ApiOkResponse,
} from '@nestjs/swagger';
import {
  NewRecordDto,
  PaginatedRecordResponseDto,
  RecordResponseDto,
  UpdateRecordDto,
} from '../dtos/record.dto';
import { RecordService } from '../services/record.service';

@ApiTags('Record')
@Controller('record')
export class RecordController {
  constructor(private readonly recordService: RecordService) {}

  @ApiCreatedResponse({ type: RecordResponseDto })
  @ApiUnauthorizedResponse()
  @UseGuards(AuthGuard('jwt'))
  @Post('new')
  public createRecord(@Req() req, @Body() record: NewRecordDto) {
    return this.recordService.createRecord(record, req.user.userId);
  }

  @ApiOkResponse({ type: PaginatedRecordResponseDto })
  @ApiUnauthorizedResponse()
  @UseGuards(AuthGuard('jwt'))
  @Get('patient/:id')
  public findByPatientId(
    @Req() req,
    @Param('id') id: string,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    return this.recordService.findByPatientId({ page, limit }, id);
  }

  @ApiOkResponse()
  @ApiUnauthorizedResponse()
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  public deleteRecord(@Req() req, @Param('id') id: string) {
    return this.recordService.deleteById(id, req.user.userId);
  }

  @ApiOkResponse({ type: RecordResponseDto })
  @ApiUnauthorizedResponse()
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  public updateRecord(
    @Req() req,
    @Param('id') id: string,
    @Body() record: UpdateRecordDto,
  ) {
    return this.recordService.updateById(id, record, req.user.userId);
  }
}
