import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RecordEntity } from './models/record.entity';
import { RecordService } from './services/record.service';
import { RecordController } from './controllers/record.controller';
import { CodetablesModule } from 'src/codetables/codetables.module';
import { EmployeeModule } from 'src/employee/employee.module';
import { PatientModule } from 'src/patient/patient.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([RecordEntity]),
    CodetablesModule,
    PatientModule,
    EmployeeModule,
  ],
  providers: [RecordService],
  controllers: [RecordController],
  exports: [RecordService],
})
export class RecordModule {}
