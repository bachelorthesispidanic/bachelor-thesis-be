import {
  ForbiddenException,
  HttpException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import {
  map,
  concatMap,
  switchMap,
  forkJoin,
  catchError,
  from,
  take,
  throwError,
  combineLatest,
} from 'rxjs';
import { DiagnoseService } from 'src/codetables/services/diagnose.service';
import { ProcedureService } from 'src/codetables/services/procedure.service';
import { EmployeeService } from 'src/employee/services/employee.service';
import { PatientService } from 'src/patient/services/patient.service';
import { Repository } from 'typeorm';
import {
  NewRecordInterface,
  UpdateRecordInterface,
} from '../interfaces/record.interface';
import { RecordEntity } from '../models/record.entity';

@Injectable()
export class RecordService {
  constructor(
    @InjectRepository(RecordEntity)
    private readonly recordRepository: Repository<RecordEntity>,
    private readonly employeeService: EmployeeService,
    private readonly patientService: PatientService,
    private readonly diagnoseService: DiagnoseService,
    private readonly procedureService: ProcedureService,
  ) {}

  // method creates record entity
  public createRecord(record: NewRecordInterface, employeeAuthId: string) {
    const newRecord = new RecordEntity();
    newRecord.description = record.description;

    return this.employeeService.findByAuthUserId(employeeAuthId).pipe(
      map((employee) => (newRecord.employee = employee)),
      concatMap(() => this.patientService.findById(record.patientId)),
      map((patient) => (newRecord.patient = patient)),
      switchMap(() => {
        const obs = record.diagnoseIds.map((diagnoseId) =>
          this.diagnoseService.findById(diagnoseId),
        );
        return forkJoin(obs);
      }),
      map((diagnoses) => (newRecord.diagnoses = diagnoses)),
      switchMap(() => {
        const obs = record.procedureIds.map((procedureId) =>
          this.procedureService.findById(procedureId),
        );
        return forkJoin(obs);
      }),
      map((procedures) => (newRecord.procedures = procedures)),
      concatMap(() => this.saveRecord(newRecord)),
      map((record) => record),
    );
  }

  // method gets paginated records by patientId
  public findByPatientId(options: IPaginationOptions, patientId: string) {
    return from(
      paginate(
        this.recordRepository
          .createQueryBuilder('record')
          .leftJoinAndSelect('record.diagnoses', 'diagnoses')
          .leftJoinAndSelect('record.procedures', 'procedures')
          .leftJoinAndSelect('record.employee', 'employee')
          .leftJoinAndSelect('record.patient', 'patient')
          .where('record.patientId = :patientId', { patientId: patientId })
          .orderBy('record.createdAt', 'DESC'),
        options,
      ),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method finds record by id
  public findById(id: string) {
    return from(
      this.recordRepository.findOne({
        where: { id: id },
        relations: ['procedures', 'diagnoses'],
      }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method deletes record by id if not older than 7 days
  public deleteById(id: string, employeeAuthId: string) {
    return this.findById(id).pipe(
      map((record) => {
        if (record.employee.authUser.id !== employeeAuthId) {
          throw new UnauthorizedException(
            'You are not authorized to delete this record',
          );
        }
        if (this.isOlderThan7Days(record)) {
          throw new ForbiddenException(
            'This record is older than 7 days and cannot be deleted',
          );
        }
        return record;
      }),
      concatMap((record) => this.recordRepository.softDelete(record)),
      map(() => 'OK'),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method updates record by id if not older than 7 days
  public updateById(
    id: string,
    recordData: UpdateRecordInterface,
    employeeAuthId: string,
  ) {
    return this.findById(id).pipe(
      map((record) => {
        if (record.employee.authUser.id !== employeeAuthId) {
          throw new UnauthorizedException(
            'You are not authorized to update this record',
          );
        }
        if (this.isOlderThan7Days(record)) {
          throw new ForbiddenException(
            'This record is older than 7 days and cannot be updated',
          );
        }
        return record;
      }),
      switchMap(() => {
        const diagnoseObs = recordData.diagnoseIds.map((diagnoseId) =>
          this.diagnoseService.findById(diagnoseId),
        );
        const procedureObs = recordData.procedureIds.map((procedureId) =>
          this.procedureService.findById(procedureId),
        );
        return combineLatest([forkJoin(diagnoseObs), forkJoin(procedureObs)]);
      }),
      concatMap(([diagnoses, procedures]) =>
        from(
          this.recordRepository.update(
            { id: id },
            {
              procedures: procedures,
              diagnoses: diagnoses,
              description: recordData.description,
            },
          ),
        ),
      ),
      map((record) => record),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  /*
   ********************
   * utility functions*
   ********************
   */

  // function to save record entity
  private saveRecord(record: RecordEntity) {
    console.log(record);
    return from(this.recordRepository.save(record)).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // function checks if record is older than 7 days
  private isOlderThan7Days(record: RecordEntity) {
    return (
      Math.ceil(
        Math.abs(
          (new Date() as unknown as number) -
            (record.createdAt as unknown as number),
        ) /
          (1000 * 60 * 60 * 24),
      ) > 7
    );
  }
}
