import { DiagnoseEntity } from 'src/codetables/models/diagnose.entity';
import { ProcedureEntity } from 'src/codetables/models/procedure.entity';
import { EmployeeEntity } from 'src/employee/models/employee.entity';
import { PatientEntity } from 'src/patient/models/patient.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  JoinTable,
  ManyToMany,
} from 'typeorm';

@Entity('record')
export class RecordEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  description: string;

  @ManyToOne(() => EmployeeEntity)
  @JoinColumn({ name: 'employeeId' })
  employee: EmployeeEntity;

  @ManyToOne(() => PatientEntity)
  @JoinColumn({ name: 'patientId' })
  patient: PatientEntity;

  @ManyToMany(() => DiagnoseEntity, { eager: true })
  @JoinTable()
  diagnoses: DiagnoseEntity[];

  @ManyToMany(() => ProcedureEntity, { eager: true })
  @JoinTable()
  procedures: ProcedureEntity[];

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    onUpdate: 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    default: null,
  })
  deletedAt: Date;
}
