export interface NewRecordInterface {
  description: string;
  patientId: string;
  diagnoseIds: string[];
  procedureIds: string[];
}

export interface UpdateRecordInterface {
  description: string;
  diagnoseIds: string[];
  procedureIds: string[];
}
