import { ApiProperty } from '@nestjs/swagger';
import { ArrayMinSize, IsArray, IsNotEmpty, IsString } from 'class-validator';
import { DiagnoseDto } from 'src/codetables/dtos/diagnose.dto';
import { ProcedureDto } from 'src/codetables/dtos/procedure.dto';
import { MetaDto } from 'src/common/common.dto';
import { EmployeeResponseDto } from 'src/employee/dtos/employee.dto';
import { PatientResponseDto } from 'src/patient/dtos/patient.dto';

export class NewRecordDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  patientId: string;

  @ApiProperty()
  @IsArray()
  @IsString({ each: true })
  @ArrayMinSize(1)
  diagnoseIds: string[];

  @ApiProperty()
  @IsArray()
  @IsString({ each: true })
  @ArrayMinSize(1)
  procedureIds: string[];
}

export class UpdateRecordDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty()
  @IsArray()
  @IsString({ each: true })
  @ArrayMinSize(1)
  diagnoseIds: string[];

  @ApiProperty()
  @IsArray()
  @IsString({ each: true })
  @ArrayMinSize(1)
  procedureIds: string[];
}

export class RecordResponseDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  employee: EmployeeResponseDto;

  @ApiProperty()
  patient: PatientResponseDto;

  @ApiProperty()
  diagnoses: DiagnoseDto[];

  @ApiProperty()
  procedures: ProcedureDto[];

  @ApiProperty()
  createdAt: string;

  @ApiProperty()
  updatedAt: string;

  @ApiProperty()
  deletedAt: string;
}

export class PaginatedRecordResponseDto {
  @ApiProperty()
  items: RecordResponseDto[];

  @ApiProperty()
  meta: MetaDto;
}
