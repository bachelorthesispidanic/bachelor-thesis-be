import { AuthUserEntity } from 'src/auth/models/auth-user.entity';
import { SexEntity } from 'src/codetables/models/sex.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EmployeeEntity } from '../../employee/models/employee.entity';

@Entity('patient')
export class PatientEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  titlesBeforeName: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ nullable: true })
  titlesAfterName: string;

  @Column()
  identificationNumber: string;

  @Column()
  birthDate: Date;

  @Column()
  phone: string;

  @Column()
  street: string;

  @Column()
  houseNumber: string;

  @Column()
  zipCode: string;

  @Column()
  city: string;

  @Column()
  country: string;

  @Column()
  insuranceCode: number;

  @Column()
  weight: number;

  @Column()
  height: number;

  @ManyToOne(() => SexEntity, { eager: true })
  @JoinColumn({ name: 'sexId' })
  sex: SexEntity;

  @ManyToOne(() => EmployeeEntity)
  @JoinColumn({ name: 'gpId' })
  gp: EmployeeEntity;

  @OneToOne(() => AuthUserEntity)
  @JoinColumn({ name: 'authUserId' })
  authUser: AuthUserEntity;

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    onUpdate: 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    default: null,
  })
  deletedAt: Date;
}
