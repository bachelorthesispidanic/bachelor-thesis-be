import { ApiProperty } from '@nestjs/swagger';
import {
  IsDate,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { SexDto } from 'src/codetables/dtos/sex.dto';
import { MetaDto } from 'src/common/common.dto';

export class NewPatientDto {
  @ApiProperty()
  @IsString()
  @IsOptional()
  titlesBeforeName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  lastName: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  titlesAfterName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  identificationNumber: string;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  birthDate: Date;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  phone: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  street: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  houseNumber: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  zipCode: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  city: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  country: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  insuranceCode: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  weight: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  height: number;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  sexId: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  gpId: string;
}

export class PatientResponseDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  titlesBeforeName: string;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  titlesAfterName: string;

  @ApiProperty()
  identificationNumber: string;

  @ApiProperty()
  birthDate: string;

  @ApiProperty()
  phone: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  street: string;

  @ApiProperty()
  houseNumber: string;

  @ApiProperty()
  zipCode: string;

  @ApiProperty()
  city: string;

  @ApiProperty()
  country: string;

  @ApiProperty()
  insuranceCode: number;

  @ApiProperty()
  weight: number;

  @ApiProperty()
  height: number;

  @ApiProperty()
  createdAt: string;

  @ApiProperty()
  updatedAt: string;

  @ApiProperty()
  deletedAt: string;

  @ApiProperty()
  sex: SexDto;
}

export class PaginatedPatientResponseDto {
  @ApiProperty()
  items: PatientResponseDto[];

  @ApiProperty()
  meta: MetaDto;
}
