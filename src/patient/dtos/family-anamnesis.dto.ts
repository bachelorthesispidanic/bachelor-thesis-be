import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';
import { MetaDto } from 'src/common/common.dto';
import { PatientResponseDto } from './patient.dto';

export class NewFamilyAnamnesisDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  patientId: string;
}

export class UpdateFamilyAnamnesisDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  description: string;
}

export class FamilyAnamnesisResponseDto {
  @ApiProperty()
  description: string;

  @ApiProperty()
  id: string;

  @ApiProperty()
  createdAt: string;

  @ApiProperty()
  updatedAt: string;

  @ApiProperty()
  deletedAt: string;

  @ApiProperty()
  patient: PatientResponseDto;
}

export class PaginatedFamilyAnamnesisResponseDto {
  @ApiProperty()
  items: FamilyAnamnesisResponseDto[];

  @ApiProperty()
  meta: MetaDto;
}
