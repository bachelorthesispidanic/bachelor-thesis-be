import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FamilyAnamnesisEntity } from './models/family-anamnesis.entity';
import { PatientEntity } from './models/patient.entity';
import { PersonalAnamnesisEntity } from './models/personal-anamnesis.entity';
import { PharmacologicalAnamnesisEntity } from './models/pharmacological-anamnesis.entity';
import { PatientController } from './controllers/patient.controller';
import { PatientService } from './services/patient.service';
import { FamilyAnamnesisService } from './services/family-anamnesis.service';
import { PersonalAnamnesisService } from './services/personal-anamnesis.service';
import { PharmacologicalAnamnesisService } from './services/pharmacological-anamnesis.service';
import { AuthModule } from 'src/auth/auth.module';
import { CodetablesModule } from 'src/codetables/codetables.module';
import { EmployeeModule } from 'src/employee/employee.module';
import { FamilyAnamnesisController } from './controllers/family-anamnesis.controller';
import { PharmacologicalAnamnesisController } from './controllers/pharmacological-anamnesis.controller';
import { PersonalAnamnesisController } from './controllers/personal-anamnesis.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      PatientEntity,
      PersonalAnamnesisEntity,
      FamilyAnamnesisEntity,
      PharmacologicalAnamnesisEntity,
    ]),
    AuthModule,
    CodetablesModule,
    EmployeeModule,
  ],
  controllers: [
    PatientController,
    FamilyAnamnesisController,
    PharmacologicalAnamnesisController,
    PersonalAnamnesisController,
  ],
  providers: [
    PatientService,
    FamilyAnamnesisService,
    PersonalAnamnesisService,
    PharmacologicalAnamnesisService,
  ],
  exports: [PatientService],
})
export class PatientModule {}
