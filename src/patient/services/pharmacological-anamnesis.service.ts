import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { take, map, concatMap, from, catchError, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { PharmacologicalAnamnesisEntity } from '../models/pharmacological-anamnesis.entity';
import { PatientService } from './patient.service';
import { NewPharmacologicalAnamnesisInterface } from '../interfaces/pharmacological-anamnesis.interface';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';

@Injectable()
export class PharmacologicalAnamnesisService {
  constructor(
    @InjectRepository(PharmacologicalAnamnesisEntity)
    private readonly pharmacologicalAnamnesisRepository: Repository<PharmacologicalAnamnesisEntity>,
    private readonly patientService: PatientService,
  ) {}

  // method finds pharmacological_anamnesis by patient_id
  public findByPatientId(options: IPaginationOptions, patientId: string) {
    return from(
      paginate(
        this.pharmacologicalAnamnesisRepository
          .createQueryBuilder('pharmacological_anamnesis')
          .leftJoinAndSelect('pharmacological_anamnesis.patient', 'patient')
          .where('patient.id = :patientId', { patientId: patientId })
          .orderBy('pharmacological_anamnesis.createdAt', 'DESC'),
        options,
      ),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method creates new pharmacological_anamnesis entity
  public createNewPharmacologicalAnamnesis(
    pharmacologicalAnamnesisData: NewPharmacologicalAnamnesisInterface,
  ) {
    const newPharmacologicalAnamnesis = new PharmacologicalAnamnesisEntity();
    newPharmacologicalAnamnesis.description =
      pharmacologicalAnamnesisData.description;

    return this.patientService
      .findById(pharmacologicalAnamnesisData.patientId)
      .pipe(
        take(1),
        map((patient) => (newPharmacologicalAnamnesis.patient = patient)),
        concatMap(() =>
          from(
            this.pharmacologicalAnamnesisRepository.save(
              newPharmacologicalAnamnesis,
            ),
          ),
        ),
        catchError((err) => {
          console.log(JSON.stringify(err));
          return throwError(
            () =>
              new HttpException(
                err.message ?? 'Unknown DB error',
                err.status ?? 500,
              ),
          );
        }),
      );
  }

  // method deletes pharmacological_anamnesis entity
  public deletePharmacologicalAnamnesis(id: string) {
    return from(
      this.pharmacologicalAnamnesisRepository.softDelete({ id: id }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method updates pharmacological_anamnesis entity
  public updatePharmacologicalAnamnesis(id: string, description: string) {
    return from(
      this.pharmacologicalAnamnesisRepository.update(
        { id: id },
        { description: description },
      ),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
