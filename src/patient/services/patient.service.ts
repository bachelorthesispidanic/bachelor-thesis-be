import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import { catchError, concatMap, from, map, take, throwError } from 'rxjs';
import { AuthService } from 'src/auth/services/auth.service';
import { SexService } from 'src/codetables/services/sex.service';
import { EmployeeService } from 'src/employee/services/employee.service';
import { Brackets, Repository } from 'typeorm';
import { NewPatientInterface } from '../interfaces/patient.interface';
import { PatientEntity } from '../models/patient.entity';

@Injectable()
export class PatientService {
  constructor(
    @InjectRepository(PatientEntity)
    private readonly patientRepository: Repository<PatientEntity>,
    private readonly authService: AuthService,
    private readonly employeeService: EmployeeService,
    private readonly sexService: SexService,
  ) {}

  // method to get all patients
  public async findAll() {
    return await this.patientRepository.find({ relations: ['authUser'] });
  }

  // method creates new patient entity
  public createNewPatient(patientData: NewPatientInterface) {
    const newPatient = new PatientEntity();
    newPatient.firstName = patientData.firstName;
    newPatient.lastName = patientData.lastName;
    newPatient.titlesBeforeName = patientData.titlesBeforeName
      ? patientData.titlesBeforeName + ' '
      : null;
    newPatient.titlesAfterName = patientData.titlesAfterName
      ? ', ' + patientData.titlesAfterName
      : null;
    newPatient.identificationNumber = patientData.identificationNumber;
    newPatient.birthDate = patientData.birthDate;
    newPatient.phone = patientData.phone;
    newPatient.street = patientData.street;
    newPatient.houseNumber = patientData.houseNumber;
    newPatient.zipCode = patientData.zipCode;
    newPatient.city = patientData.city;
    newPatient.country = patientData.country;
    newPatient.insuranceCode = patientData.insuranceCode;
    newPatient.weight = patientData.weight;
    newPatient.height = patientData.height;

    return this.sexService.findById(patientData.sexId).pipe(
      map((sex) => (newPatient.sex = sex)),
      concatMap(() => this.employeeService.findById(patientData.gpId)),
      map((employee) => (newPatient.gp = employee)),
      concatMap(() => this.authService.createAuthUser(patientData.email)),
      map((authUser) => (newPatient.authUser = authUser)),
      concatMap(() => this.savePatient(newPatient)),
    );
  }

  // method finds patient by id
  public findById(id: string) {
    return from(
      this.patientRepository.findOne({
        where: { id: id },
      }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method finds patient by authUserId
  public findByAuthUserId(authUserId: string) {
    return from(
      this.patientRepository.findOne({
        where: { authUser: { id: authUserId } },
      }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method gets patients details
  public getPatientDetails(patientId: string) {
    return from(
      this.patientRepository.findOne({
        where: { id: patientId },
        relations: ['authUser'],
      }),
    ).pipe(
      take(1),
      map((res) => {
        const email = res.authUser.email;
        delete res.authUser;
        res['email'] = email;
        return res;
      }),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method finds patient by identificationNumber
  public findByIdentificationNumber(identificationNumber: string) {
    return from(
      this.patientRepository.findOne({
        where: { identificationNumber: identificationNumber },
      }),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method gets paginated patients by gpId and searchTerm
  public getDoctorsPatients(
    options: IPaginationOptions,
    employeeAuthId: string,
    searchTerm: string,
  ) {
    return this.employeeService
      .findByAuthUserId(employeeAuthId)
      .pipe(
        concatMap((employee) =>
          this.findByGpId(options, employee.id, searchTerm),
        ),
      );
  }

  /*
   ********************
   * utility functions*
   ********************
   */

  // function to save patient entity
  private savePatient(patient: PatientEntity) {
    return from(this.patientRepository.save(patient)).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // function finds patients by gpId
  private findByGpId(
    options: IPaginationOptions,
    gpId: string,
    searchTerm: string,
  ) {
    return from(
      paginate(
        this.patientRepository
          .createQueryBuilder('patient')
          .select([
            'patient.id',
            'patient.firstName',
            'patient.lastName',
            'patient.birthDate',
            'patient.identificationNumber',
          ])
          .where({
            gp: { id: gpId },
          })
          .andWhere(
            new Brackets((qb) => {
              qb.where('patient.firstName ILIKE :searchTerm', {
                searchTerm: `%${searchTerm}%`,
              })
                .orWhere('patient.lastName ILIKE :searchTerm', {
                  searchTerm: `%${searchTerm}%`,
                })
                .orWhere('patient.identificationNumber ILIKE :searchTerm', {
                  searchTerm: `%${searchTerm}%`,
                });
            }),
          )
          .orderBy('patient.lastName', 'ASC'),
        options,
      ),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
