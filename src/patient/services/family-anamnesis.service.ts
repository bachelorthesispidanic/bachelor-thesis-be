import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import { catchError, concatMap, from, map, take, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { NewFamilyAnamnesisInterface } from '../interfaces/family-anamnesis.interface';
import { FamilyAnamnesisEntity } from '../models/family-anamnesis.entity';
import { PatientService } from './patient.service';

@Injectable()
export class FamilyAnamnesisService {
  constructor(
    @InjectRepository(FamilyAnamnesisEntity)
    private readonly familyAnamnesisRepository: Repository<FamilyAnamnesisEntity>,
    private readonly patientService: PatientService,
  ) {}

  // method finds family_anamnesis by patient_id
  public findByPatientId(options: IPaginationOptions, patientId: string) {
    return from(
      paginate(
        this.familyAnamnesisRepository
          .createQueryBuilder('family_anamnesis')
          .leftJoinAndSelect('family_anamnesis.patient', 'patient')
          .where('patient.id = :patientId', {
            patientId: patientId,
          })
          .orderBy('family_anamnesis.createdAt', 'DESC'),
        options,
      ),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method creates new family_anamnesis entity
  public createNewFamilyAnamnesis(
    familyAnamnesisData: NewFamilyAnamnesisInterface,
  ) {
    const newFamilyAnamnesis = new FamilyAnamnesisEntity();
    newFamilyAnamnesis.description = familyAnamnesisData.description;

    return this.patientService.findById(familyAnamnesisData.patientId).pipe(
      take(1),
      map((patient) => (newFamilyAnamnesis.patient = patient)),
      concatMap(() =>
        from(this.familyAnamnesisRepository.save(newFamilyAnamnesis)),
      ),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method deletes family_anamnesis entity
  public deleteFamilyAnamnesis(id: string) {
    return from(this.familyAnamnesisRepository.softDelete({ id: id })).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method updates family_anamnesis entity
  public updateFamilyAnamnesis(id: string, description: string) {
    return from(
      this.familyAnamnesisRepository.update(
        { id: id },
        { description: description },
      ),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
