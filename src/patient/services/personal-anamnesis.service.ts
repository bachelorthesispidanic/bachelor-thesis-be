import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import { take, map, concatMap, from, catchError, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { NewPersonalAnamnesisInterface } from '../interfaces/personal-anamnesis.interface';
import { PersonalAnamnesisEntity } from '../models/personal-anamnesis.entity';
import { PatientService } from './patient.service';

@Injectable()
export class PersonalAnamnesisService {
  constructor(
    @InjectRepository(PersonalAnamnesisEntity)
    private readonly personalAnamnesisRepository: Repository<PersonalAnamnesisEntity>,
    private readonly patientService: PatientService,
  ) {}

  // method finds personal_anamnesis by patient_id
  public findByPatientId(options: IPaginationOptions, patientId: string) {
    return from(
      paginate(
        this.personalAnamnesisRepository
          .createQueryBuilder('personal_anamnesis')
          .leftJoinAndSelect('personal_anamnesis.patient', 'patient')
          .where('patient.id = :patientId', { patientId: patientId })
          .orderBy('personal_anamnesis.createdAt', 'DESC'),
        options,
      ),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method creates new personal_anamnesis entity
  public createNewPersonalAnamnesis(
    personalAnamnesisData: NewPersonalAnamnesisInterface,
  ) {
    const newPersonalAnamnesis = new PersonalAnamnesisEntity();
    newPersonalAnamnesis.description = personalAnamnesisData.description;

    return this.patientService.findById(personalAnamnesisData.patientId).pipe(
      take(1),
      map((patient) => (newPersonalAnamnesis.patient = patient)),
      concatMap(() =>
        from(this.personalAnamnesisRepository.save(newPersonalAnamnesis)),
      ),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method deletes personal_anamnesis entity
  public deletePersonalAnamnesis(id: string) {
    return from(this.personalAnamnesisRepository.softDelete({ id: id })).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // method updates personal_anamnesis entity
  public updatePersonalAnamnesis(id: string, description: string) {
    return from(
      this.personalAnamnesisRepository.update(
        { id: id },
        { description: description },
      ),
    ).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }
}
