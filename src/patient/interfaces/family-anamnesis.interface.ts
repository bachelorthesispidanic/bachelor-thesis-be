export interface NewFamilyAnamnesisInterface {
  patientId: string;
  description: string;
}
