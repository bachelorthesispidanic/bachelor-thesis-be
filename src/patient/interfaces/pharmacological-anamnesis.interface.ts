export interface NewPharmacologicalAnamnesisInterface {
  patientId: string;
  description: string;
}
