export interface NewPersonalAnamnesisInterface {
  patientId: string;
  description: string;
}
