export interface NewPatientInterface {
  titlesBeforeName: string;
  firstName: string;
  lastName: string;
  titlesAfterName: string;
  identificationNumber: string;
  birthDate: Date;
  email: string;
  phone: string;
  street: string;
  houseNumber: string;
  zipCode: string;
  city: string;
  country: string;
  insuranceCode: number;
  weight: number;
  height: number;
  sexId: string;
  gpId: string;
}
