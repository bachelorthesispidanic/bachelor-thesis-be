import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiOkResponse, ApiCreatedResponse } from '@nestjs/swagger';
import {
  FamilyAnamnesisResponseDto,
  NewFamilyAnamnesisDto,
  PaginatedFamilyAnamnesisResponseDto,
  UpdateFamilyAnamnesisDto,
} from '../dtos/family-anamnesis.dto';
import { FamilyAnamnesisService } from '../services/family-anamnesis.service';

@ApiTags('FamilyAnamnesis')
@Controller('family-anamnesis')
export class FamilyAnamnesisController {
  constructor(
    private readonly familyAnamnesisService: FamilyAnamnesisService,
  ) {}

  @ApiOkResponse({ type: PaginatedFamilyAnamnesisResponseDto })
  @Get('patient/:id')
  getPatientsFamilyAnamnesis(
    @Param('id') id: string,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    return this.familyAnamnesisService.findByPatientId({ page, limit }, id);
  }

  @ApiCreatedResponse({ type: FamilyAnamnesisResponseDto })
  @Post('')
  createNewFamilyAnamnesis(@Body() familyAnamnesisData: NewFamilyAnamnesisDto) {
    return this.familyAnamnesisService.createNewFamilyAnamnesis(
      familyAnamnesisData,
    );
  }

  @ApiOkResponse({ type: FamilyAnamnesisResponseDto })
  @Patch(':id')
  updateFamilyAnamnesis(
    @Param('id') id: string,
    @Body() familyAnamnesisData: UpdateFamilyAnamnesisDto,
  ) {
    return this.familyAnamnesisService.updateFamilyAnamnesis(
      id,
      familyAnamnesisData.description,
    );
  }

  @ApiOkResponse()
  @Delete(':id')
  deleteFamilyAnamnesis(@Param('id') id: string) {
    return this.familyAnamnesisService.deleteFamilyAnamnesis(id);
  }
}
