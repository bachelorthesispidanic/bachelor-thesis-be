import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import {
  NewPersonalAnamnesisDto,
  PaginatedPersonalAnamnesisResponseDto,
  PersonalAnamnesisResponseDto,
  UpdatePersonalAnamnesisDto,
} from '../dtos/personal-anamnesis.dto';
import { PersonalAnamnesisService } from '../services/personal-anamnesis.service';

@ApiTags('PersonalAnamnesis')
@Controller('personal-anamnesis')
export class PersonalAnamnesisController {
  constructor(
    private readonly personalAnamnesisService: PersonalAnamnesisService,
  ) {}

  @ApiOkResponse({ type: PaginatedPersonalAnamnesisResponseDto })
  @Get('patient/:id')
  getPatientsPersonalAnamnesis(
    @Param('id') id: string,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    return this.personalAnamnesisService.findByPatientId({ page, limit }, id);
  }

  @ApiCreatedResponse({ type: PersonalAnamnesisResponseDto })
  @Post('')
  createNewPersonalAnamnesis(
    @Body() personalAnamnesisData: NewPersonalAnamnesisDto,
  ) {
    return this.personalAnamnesisService.createNewPersonalAnamnesis(
      personalAnamnesisData,
    );
  }

  @ApiOkResponse({ type: PersonalAnamnesisResponseDto })
  @Patch(':id')
  updatePersonalAnamnesis(
    @Param('id') id: string,
    @Body() personalAnamnesisData: UpdatePersonalAnamnesisDto,
  ) {
    return this.personalAnamnesisService.updatePersonalAnamnesis(
      id,
      personalAnamnesisData.description,
    );
  }

  @ApiOkResponse()
  @Delete(':id')
  deletePersonalAnamnesis(@Param('id') id: string) {
    return this.personalAnamnesisService.deletePersonalAnamnesis(id);
  }
}
