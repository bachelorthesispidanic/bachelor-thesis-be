import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiOkResponse, ApiCreatedResponse } from '@nestjs/swagger';
import {
  NewPharmacologicalAnamnesisDto,
  PaginatedPharmacologicalAnamnesisResponseDto,
  PharmacologicalAnamnesisResponseDto,
  UpdatePharmacologicalAnamnesisDto,
} from '../dtos/pharmacological-anamnesis.dto';
import { PharmacologicalAnamnesisService } from '../services/pharmacological-anamnesis.service';

@ApiTags('PharmacologicalAnamnesis')
@Controller('pharmacological-anamnesis')
export class PharmacologicalAnamnesisController {
  constructor(
    private readonly pharmacologicalAnamnesisService: PharmacologicalAnamnesisService,
  ) {}

  @ApiOkResponse({ type: PaginatedPharmacologicalAnamnesisResponseDto })
  @Get('patient/:id')
  getPatientsPharmacologicalAnamnesis(
    @Param('id') id: string,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
  ) {
    return this.pharmacologicalAnamnesisService.findByPatientId(
      { page, limit },
      id,
    );
  }

  @ApiCreatedResponse({ type: PharmacologicalAnamnesisResponseDto })
  @Post('')
  createNewPharmacologicalAnamnesis(
    @Body() pharmacologicalAnamnesisData: NewPharmacologicalAnamnesisDto,
  ) {
    return this.pharmacologicalAnamnesisService.createNewPharmacologicalAnamnesis(
      pharmacologicalAnamnesisData,
    );
  }

  @ApiOkResponse({ type: PharmacologicalAnamnesisResponseDto })
  @Patch(':id')
  updatePharmacologicalAnamnesis(
    @Param('id') id: string,
    @Body() pharmacologicalAnamnesisData: UpdatePharmacologicalAnamnesisDto,
  ) {
    return this.pharmacologicalAnamnesisService.updatePharmacologicalAnamnesis(
      id,
      pharmacologicalAnamnesisData.description,
    );
  }

  @ApiOkResponse()
  @Delete(':id')
  deletePharmacologicalAnamnesis(@Param('id') id: string) {
    return this.pharmacologicalAnamnesisService.deletePharmacologicalAnamnesis(
      id,
    );
  }
}
