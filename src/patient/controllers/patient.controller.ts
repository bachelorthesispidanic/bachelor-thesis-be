import {
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiTags,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  NewPatientDto,
  PaginatedPatientResponseDto,
  PatientResponseDto,
} from '../dtos/patient.dto';
import { PatientService } from '../services/patient.service';

@ApiTags('Patient')
@Controller('patient')
export class PatientController {
  constructor(private readonly patientService: PatientService) {}

  @ApiCreatedResponse({ type: PatientResponseDto })
  @Post('new')
  createNewPatient(@Body() patientData: NewPatientDto) {
    return this.patientService.createNewPatient(patientData);
  }

  @ApiOkResponse({ type: PaginatedPatientResponseDto })
  @ApiUnauthorizedResponse()
  @UseGuards(AuthGuard('jwt'))
  @Get('doctor')
  getDoctorsPatients(
    @Req() req,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
    @Query('search', new DefaultValuePipe('')) search: string,
  ) {
    return this.patientService.getDoctorsPatients(
      { page, limit },
      req.user.userId,
      search,
    );
  }

  @ApiOkResponse({ type: PatientResponseDto })
  @Get(':id')
  getPatient(@Param('id') id: string) {
    return this.patientService.getPatientDetails(id);
  }
}
