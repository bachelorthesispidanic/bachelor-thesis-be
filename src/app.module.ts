import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { RedisModule } from './redis/redis.module';
import { MailModule } from './mail/mail.module';
import { PatientModule } from './patient/patient.module';
import { SeedModule } from './seed/seed.module';
import { PostgresProviderModule } from './provider/provider.module';
import { CodetablesModule } from './codetables/codetables.module';
import { EmployeeModule } from './employee/employee.module';
import { RecordModule } from './record/record.module';
import { PrescriptionModule } from './prescription/prescription.module';
import { AppointmentModule } from './appointment/appointment.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env'],
    }),
    AuthModule,
    RedisModule,
    MailModule,
    PatientModule,
    PostgresProviderModule,
    SeedModule,
    CodetablesModule,
    EmployeeModule,
    RecordModule,
    PrescriptionModule,
    AppointmentModule,
  ],
})
export class AppModule {}
