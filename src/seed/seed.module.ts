import { Logger, Module } from '@nestjs/common';
import { SeedService } from './services/seed.service';
import { PostgresProviderModule } from 'src/provider/provider.module';
import { CodetablesModule } from 'src/codetables/codetables.module';
import { EmployeeModule } from 'src/employee/employee.module';
import { AuthModule } from 'src/auth/auth.module';
import { PatientModule } from 'src/patient/patient.module';

@Module({
  imports: [
    PostgresProviderModule,
    CodetablesModule,
    EmployeeModule,
    AuthModule,
    PatientModule,
  ],
  providers: [SeedService, Logger],
  exports: [SeedService],
})
export class SeedModule {}
