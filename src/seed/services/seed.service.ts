import { Injectable } from '@nestjs/common';
import { NewMedicineInterface } from 'src/codetables/interfaces/medicine.interface';
import { DiagnoseService } from 'src/codetables/services/diagnose.service';
import { ExpertiseService } from 'src/codetables/services/expertise.service';
import { MedicTypeService } from 'src/codetables/services/medic-type.service';
import { MedicalNeedService } from 'src/codetables/services/medical-need.service';
import { MedicineService } from 'src/codetables/services/medicine.service';
import { ProcedureService } from 'src/codetables/services/procedure.service';
import * as DIAGNOSE_SEED from '../../../data/zoznam-diagnoz.json';
import * as EXPERTISE_SEED from '../../../data/zdravotnicka-odbornost.json';
import * as MEDIC_TYPE_SEED from '../../../data/typ-pracovnika.json';
import * as MEDICAL_NEED_SEED from '../../../data/zdravotne-pomocky.json';
import * as MEDICINE_SEED from '../../../data/zoznam-liekov.json';
import * as PROCEDURE_SEED from '../../../data/zoznam-zdravotnych-vykonov.json';
import * as SEX_SEED from '../../../data/pohlavie.json';
import { NewDiagnoseInterface } from 'src/codetables/interfaces/diagnose.interface';
import { NewExpertiseInterface } from 'src/codetables/interfaces/expertise.interface';
import { NewMedicTypeInterface } from 'src/codetables/interfaces/medic-type.interface';
import { NewMedicalNeedInterface } from 'src/codetables/interfaces/medical-need.interface';
import { NewProcedureInterface } from 'src/codetables/interfaces/procedure.interface';
import { NewSexInterface } from 'src/codetables/interfaces/sex.interface';
import { SexService } from 'src/codetables/services/sex.service';
import { EmployeeService } from 'src/employee/services/employee.service';
import { lastValueFrom } from 'rxjs';
import { faker } from '@faker-js/faker';
import { NewEmployeeInterface } from 'src/employee/interfaces/employee.interface';
import { AuthService } from 'src/auth/services/auth.service';
import { NewPatientInterface } from 'src/patient/interfaces/patient.interface';
import { PatientService } from 'src/patient/services/patient.service';

@Injectable()
export class SeedService {
  constructor(
    private readonly diagnoseService: DiagnoseService,
    private readonly expertiseService: ExpertiseService,
    private readonly medicTypeService: MedicTypeService,
    private readonly medicalNeedService: MedicalNeedService,
    private readonly medicineService: MedicineService,
    private readonly procedureService: ProcedureService,
    private readonly sexService: SexService,
    private readonly employeeService: EmployeeService,
    private readonly authService: AuthService,
    private readonly patientService: PatientService,
  ) {}

  public async seed() {
    // await this.seedDiagnose();
    // await this.seedExpertise();
    // await this.seedMedicType();
    // await this.seedMedicalNeed();
    // await this.seedMedicine();
    // await this.seedProcedure();
    // await this.seedSex();
    // await this.seedEmployee();
    // await this.seedPatients();
  }

  private async seedDiagnose() {
    const jsonData = JSON.stringify(DIAGNOSE_SEED);
    const data: NewDiagnoseInterface[] = JSON.parse(jsonData);

    return await this.diagnoseService.createMultiple(data);
  }

  private async seedExpertise() {
    const jsonData = JSON.stringify(EXPERTISE_SEED);
    const data: NewExpertiseInterface[] = JSON.parse(jsonData);

    return await this.expertiseService.createMultiple(data);
  }

  private async seedMedicType() {
    const jsonData = JSON.stringify(MEDIC_TYPE_SEED);
    const data: NewMedicTypeInterface[] = JSON.parse(jsonData);

    return await this.medicTypeService.createMultiple(data);
  }

  private async seedMedicalNeed() {
    const jsonData = JSON.stringify(MEDICAL_NEED_SEED);
    const data: NewMedicalNeedInterface[] = JSON.parse(jsonData);

    return await this.medicalNeedService.createMultiple(data);
  }

  private async seedMedicine() {
    const medicineJsonData = JSON.stringify(MEDICINE_SEED);
    const medicineData: NewMedicineInterface[] = JSON.parse(medicineJsonData);

    return await this.medicineService.createMultiple(medicineData);
  }

  private async seedProcedure() {
    const jsonData = JSON.stringify(PROCEDURE_SEED);
    const data: NewProcedureInterface[] = JSON.parse(jsonData);

    return await this.procedureService.createMultiple(data);
  }

  private async seedSex() {
    const jsonData = JSON.stringify(SEX_SEED);
    const data: NewSexInterface[] = JSON.parse(jsonData);

    return await this.sexService.createMultiple(data);
  }

  private async seedEmployee() {
    for (let i = 0; i < 10; i++) {
      const employee: NewEmployeeInterface = {
        email: faker.internet.email(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        code: `A${Math.floor(Math.random() * (99999 - 1) + 1)
          .toString()
          .padStart(5, '0')}${Math.floor(Math.random() * (116 - 1) + 1)
          .toString()
          .padStart(3, '0')}`,
        titlesBeforeName: 'Mudr.',
      };
      await lastValueFrom(this.employeeService.createNewEmployee(employee));
    }
    return await this.registerEmployees();
  }

  private async registerEmployees() {
    const employees = await this.employeeService.findAll();

    for (const employee of employees) {
      await lastValueFrom(
        this.authService.register({
          email: employee.authUser.email,
          password: 'password',
        }),
      );
    }
  }

  private async seedPatients() {
    const employeesEntities = await this.employeeService.findAll();
    const sexEntities = await this.sexService.findAll();
    const insuranceCodes = [24, 25, 27];

    for (let i = 0; i < 1000; i++) {
      const sexes = ['muž', 'žena'];
      const sexId = Math.floor(Math.random() * sexes.length);
      const sexEntityId = sexEntities.find(
        (sex) => sex.name === sexes[sexId],
      ).id;

      const start = new Date(1954, 0, 1);
      const end = new Date(2004, 0, 1);
      const randomDate = new Date(
        start.getTime() +
          Math.random() * (end.getTime() - start.getTime()) +
          start.getTime(),
      );
      const day = randomDate.getDate();
      const month = randomDate.getMonth() + 1;
      const year = randomDate.getFullYear();
      const identificationNumber = `${year.toString().slice(-2)}${(
        month + (0 === sexId ? 0 : 50)
      )
        .toString()
        .padStart(2, '0')}${day.toString().padStart(2, '0')}${(
        Math.random() * (999 - 1) +
        1
      )
        .toFixed()
        .toString()
        .padStart(4, '0')}`;
      const patient: NewPatientInterface = {
        titlesBeforeName: '',
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        titlesAfterName: '',
        identificationNumber: identificationNumber,
        birthDate: new Date(year, month - 1, day),
        email: faker.internet.email(),
        phone: faker.phone.phoneNumber(),
        street: faker.address.streetAddress(),
        houseNumber: `${(Math.random() * (999 - 1) + 1).toFixed().toString()}`,
        zipCode: faker.address.zipCode(),
        city: faker.address.city(),
        country: 'Slovakia',
        insuranceCode:
          insuranceCodes[Math.floor(Math.random() * insuranceCodes.length)],
        weight: Math.floor(Math.random() * (120 - 45) + 45),
        height: Math.floor(Math.random() * (200 - 150) + 150),
        sexId: sexEntityId,
        gpId: employeesEntities[
          Math.floor(Math.random() * employeesEntities.length)
        ].id,
      };
      await lastValueFrom(this.patientService.createNewPatient(patient));
    }

    return await this.registerPatients();
  }

  private async registerPatients() {
    const patients = await this.patientService.findAll();

    for (const patient of patients) {
      await lastValueFrom(
        this.authService.register({
          email: patient.authUser.email,
          password: 'password',
        }),
      );
    }
  }
}
