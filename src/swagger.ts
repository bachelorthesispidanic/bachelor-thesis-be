import { INestApplication, Module } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { writeFile } from 'fs/promises';
import { AppointmentController } from './appointment/controllers/appointment.controller';
import { AppointmentService } from './appointment/services/appointment.service';
import { AuthController } from './auth/controllers/auth.controller';
import { AuthService } from './auth/services/auth.service';
import { DiagnoseController } from './codetables/controllers/diagnose.controller';
import { MedicalNeedController } from './codetables/controllers/medical-need.controller';
import { MedicineController } from './codetables/controllers/medicine.controller';
import { ProcedureController } from './codetables/controllers/procedure.controller';
import { SexController } from './codetables/controllers/sex.controller';
import { DiagnoseService } from './codetables/services/diagnose.service';
import { ExpertiseService } from './codetables/services/expertise.service';
import { MedicTypeService } from './codetables/services/medic-type.service';
import { MedicalNeedService } from './codetables/services/medical-need.service';
import { MedicineService } from './codetables/services/medicine.service';
import { ProcedureService } from './codetables/services/procedure.service';
import { SexService } from './codetables/services/sex.service';
import { EmployeeController } from './employee/controllers/employee.controller';
import { EmployeeService } from './employee/services/employee.service';
import { FamilyAnamnesisController } from './patient/controllers/family-anamnesis.controller';
import { PatientController } from './patient/controllers/patient.controller';
import { PersonalAnamnesisController } from './patient/controllers/personal-anamnesis.controller';
import { PharmacologicalAnamnesisController } from './patient/controllers/pharmacological-anamnesis.controller';
import { FamilyAnamnesisService } from './patient/services/family-anamnesis.service';
import { PatientService } from './patient/services/patient.service';
import { PersonalAnamnesisService } from './patient/services/personal-anamnesis.service';
import { PharmacologicalAnamnesisService } from './patient/services/pharmacological-anamnesis.service';
import { PrescriptionController } from './prescription/controllers/prescription.controller';
import { PrescriptionService } from './prescription/services/prescription.service';
import { RecordController } from './record/controllers/record.controller';
import { RecordService } from './record/services/record.service';
import { RedisService } from './redis/services/redis.service';

// @nestjs/swagger requires running application. However, it's not possible to start the application
// if there are dependecies on external services like mongoDB, rabbitmq, etc.
// Solution is to compose an app which contains just REST controllers.
@Module({
  imports: [],
  controllers: [
    AuthController,
    RecordController,
    PrescriptionController,
    FamilyAnamnesisController,
    PatientController,
    PersonalAnamnesisController,
    PharmacologicalAnamnesisController,
    EmployeeController,
    DiagnoseController,
    MedicalNeedController,
    MedicineController,
    SexController,
    ProcedureController,
    AppointmentController,
  ],
  providers: [
    {
      provide: AuthService,
      useValue: {},
    },
    {
      provide: AppointmentService,
      useValue: {},
    },
    {
      provide: RecordService,
      useValue: {},
    },
    {
      provide: PrescriptionService,
      useValue: {},
    },
    {
      provide: FamilyAnamnesisService,
      useValue: {},
    },
    {
      provide: PatientService,
      useValue: {},
    },
    {
      provide: PersonalAnamnesisService,
      useValue: {},
    },
    {
      provide: PharmacologicalAnamnesisService,
      useValue: {},
    },
    {
      provide: EmployeeService,
      useValue: {},
    },
    {
      provide: DiagnoseService,
      useValue: {},
    },
    {
      provide: MedicalNeedService,
      useValue: {},
    },
    {
      provide: MedicineService,
      useValue: {},
    },
    {
      provide: SexService,
      useValue: {},
    },
    {
      provide: ProcedureService,
      useValue: {},
    },
    {
      provide: ExpertiseService,
      useValue: {},
    },
    {
      provide: MedicTypeService,
      useValue: {},
    },
    {
      provide: RedisService,
      useValue: {},
    },
  ],
})
export class OnlyControllersModule {}

async function generateSwaggerDocs(app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle('Bachelor thesis API')
    .setDescription(
      'API description for the Bachelor thesis of the Faculty of Informatics and Information Technologies at the Slovak University of Technologies in Bratislava.',
    )
    .setVersion('1.0')
    .addServer('http://localhost:8000')
    .build();
  const spec = SwaggerModule.createDocument(app, config);

  return writeFile('openapi.json', JSON.stringify(spec));
}

async function bootstrap() {
  const app = await NestFactory.create(OnlyControllersModule);
  return generateSwaggerDocs(app);
}

bootstrap();
