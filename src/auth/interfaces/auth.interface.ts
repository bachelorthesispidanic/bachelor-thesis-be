export interface LoginCredentialsInterface {
  email: string;
  password: string;
}

export interface NewAuthUserInterface {
  email: string;
  passwordHash: string;
  passwordSalt: string;
}

export interface AuthUserInterface {
  id: number;
  email: string;
  passwordHash: string;
  passwordSalt: string;
  isAccountActive: boolean;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}
