import {
  ForbiddenException,
  HttpException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { RedisService } from 'src/redis/services/redis.service';
import { LoginCredentialsInterface } from '../interfaces/auth.interface';
import * as bcrypt from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AuthUserEntity } from '../models/auth-user.entity';
import { catchError, concatMap, from, map, of, take, throwError } from 'rxjs';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private readonly redisService: RedisService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    @InjectRepository(AuthUserEntity)
    private readonly authUserRepository: Repository<AuthUserEntity>,
  ) {}

  // method creates auth user entity
  public createAuthUser(email: string) {
    const newAuthUser = new AuthUserEntity();
    newAuthUser.email = email;
    newAuthUser.passwordSalt = null;
    newAuthUser.passwordHash = null;
    newAuthUser.isAccountActive = false;

    return this.saveAuthUser(newAuthUser);
  }

  // method updates auth user entity with new password hash and salt and activates it
  public register(credentials: LoginCredentialsInterface) {
    return from(
      this.authUserRepository.findOneOrFail({ email: credentials.email }),
    ).pipe(
      take(1),
      concatMap((user) => {
        if (user.isAccountActive) {
          throw new ForbiddenException('Account already activated');
        }

        const passwordSalt = this.getPasswordSalt();
        const passwordHash = this.getPasswordHash(
          credentials.password + passwordSalt,
        );

        return from(
          this.authUserRepository.update(
            { email: credentials.email },
            {
              email: credentials.email,
              passwordSalt: passwordSalt,
              passwordHash: passwordHash,
              isAccountActive: true,
            },
          ),
        );
      }),
      map(() => 'OK'),
      catchError((err) => {
        console.log(err);
        return throwError(
          () =>
            new HttpException(
              err?.message ?? 'Unknown error',
              err?.status ?? 500,
            ),
        );
      }),
    );
  }

  // login method
  public login(credentials: LoginCredentialsInterface) {
    return from(
      this.authUserRepository.findOne({
        select: ['id', 'email', 'passwordHash', 'passwordSalt'],
        where: { email: credentials.email },
      }),
    ).pipe(
      take(1),
      map((authUser) => {
        if (
          authUser &&
          this.comparePasswordHash(
            credentials.password + authUser.passwordSalt,
            authUser.passwordHash,
          )
        ) {
          return this.generateTokens({
            userId: authUser.id,
            email: authUser.email,
          });
        } else {
          throw new UnauthorizedException('Invalid credentials');
        }
      }),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // logout method
  public logout(accessToken: string, refreshToken: string) {
    return this.expireToken(accessToken, 'ACCESS').pipe(
      take(1),
      concatMap(() => this.expireToken(refreshToken, 'REFRESH')),
      map(() => ({ status: 200, message: 'OK' })),
    );
  }

  // refresh token method
  public refreshToken(refreshToken: string) {
    const decodedRefreshToken = this.jwtService.decode(refreshToken);
    const payload = {
      userId: decodedRefreshToken['userId'],
      email: decodedRefreshToken['email'],
    };
    const accessToken = this.jwtService.sign(payload, {
      secret: this.configService.get<string>('ACCESS_TOKEN_SECRET'),
      expiresIn: +this.configService.get<string>('ACCESS_TOKEN_EXPIRATION'),
    });

    return { accessToken: accessToken };
  }

  /*
   ********************
   * utility functions*
   ********************
   */

  // function saves auth user entity to repository
  private saveAuthUser(authUser) {
    return from(this.authUserRepository.save(authUser)).pipe(
      take(1),
      catchError((err) => {
        console.log(JSON.stringify(err));
        return throwError(
          () =>
            new HttpException(
              err.message ?? 'Unknown DB error',
              err.status ?? 500,
            ),
        );
      }),
    );
  }

  // function returns bcrypt hash of password
  private getPasswordHash(password: string): string {
    return bcrypt.hashSync(password, 10);
  }

  // function returns bcrypt salt for password
  private getPasswordSalt(): string {
    return bcrypt.genSaltSync();
  }

  // function compares bcrypt password hash with password and returns boolean
  private comparePasswordHash(password: string, hash: string): boolean {
    return bcrypt.compareSync(password, hash);
  }

  // function generates access and refresh tokens
  private generateTokens(payload: { userId: string; email: string }) {
    const accessToken = this.jwtService.sign(payload, {
      secret: this.configService.get<string>('ACCESS_TOKEN_SECRET'),
      expiresIn: +this.configService.get<string>('ACCESS_TOKEN_EXPIRATION'),
    });

    const refreshToken = this.jwtService.sign(payload, {
      secret: this.configService.get<string>('REFRESH_TOKEN_SECRET'),
      expiresIn: +this.configService.get<string>('REFRESH_TOKEN_EXPIRATION'),
    });

    return {
      accessToken: accessToken,
      refreshToken: refreshToken,
      email: payload.email,
      userId: payload.userId,
    };
  }

  // function expires token and always returns 'OK'
  private expireToken(token: string, type: string) {
    // verify token
    try {
      this.jwtService.verify(token, {
        secret: this.configService.get<string>(`${type}_TOKEN_SECRET`),
      });
    } catch (error) {
      console.log(`Invalid ${type} token ${token}`);
      console.log(error);
      return of('OK');
    }

    // get token expiration and calculate ttl
    const expiration = this.jwtService.decode(token)['exp'];
    const ttl = parseInt((expiration - Date.now() / 1000).toFixed(0));

    // set token to redis
    return this.redisService.set(token, '1', ttl).pipe(
      take(1),
      map(() => of('OK')),
      catchError((error) => {
        console.log(`Failed to expire ${type} token ${token}`);
        console.log(error);
        return of('OK');
      }),
    );
  }
}
