import {
  Body,
  Controller,
  HttpCode,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiForbiddenResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  LoginCredentialsDto,
  LoginResponseDto,
  RefreshTokenDto,
  RefreshTokenResponseDto,
  RegisterBodyDto,
} from '../dtos/auth.dto';
import { AuthService } from '../services/auth.service';
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOkResponse()
  @ApiForbiddenResponse()
  @HttpCode(200)
  @Post('register')
  register(@Body() data: RegisterBodyDto) {
    return this.authService.register(data);
  }

  @ApiOkResponse({ type: LoginResponseDto })
  @ApiUnauthorizedResponse()
  @HttpCode(200)
  @Post('login')
  login(@Body() credentials: LoginCredentialsDto) {
    return this.authService.login(credentials);
  }

  @ApiOkResponse()
  @Post('logout')
  @HttpCode(200)
  logout(@Req() req, @Body() data: RefreshTokenDto) {
    const accessToken = req.headers['authorization'].split(' ')[1];

    return this.authService.logout(accessToken, data.refreshToken);
  }

  @ApiOkResponse({ type: RefreshTokenResponseDto })
  @ApiUnauthorizedResponse()
  @UseGuards(AuthGuard('jwt-refresh'))
  @HttpCode(200)
  @Post('refresh-token')
  refreshToken(@Body() data: RefreshTokenDto) {
    return this.authService.refreshToken(data.refreshToken);
  }
}
